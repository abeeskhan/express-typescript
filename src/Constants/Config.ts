// Configure Server Properties here
export const SERVER = {
  HOSTNAME: 'localhost',
  PORT: 3000,
  BASE_URL: '/api',
  CORS_LOCATION: '*',
  CORS_HEADERS: 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
};

// Logging Configuration
export const LOGGING = {
  ENABLED: false,
  FOLDER: './logs',
  INFO_LOG_FILE: 'info.log',
  ERROR_LOG_FILE: 'error.log',
};
