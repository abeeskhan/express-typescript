import { Router } from 'express';
import { userHandler } from '../../Handler';
import { ROUTES } from './UserRoute.Constants';

const userRouter: Router = Router();

userRouter.post(ROUTES.GET_ALL, userHandler.getAllUsers);

export default userRouter;
