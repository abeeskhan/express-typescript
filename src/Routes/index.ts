import { Router } from 'express';
import { SERVER } from '../Constants/Config';
import userRouter from './UserRoute';

const router: Router = Router();

router.use(SERVER.BASE_URL, userRouter);

export default router;
