import { SERVER } from './Constants/Config';
import express, { Application, Request, Response, NextFunction } from 'express';
import chalk from 'chalk';
import logger from './Logger';
import bodyParser from 'body-parser';
import ip from 'ip';
import router from './Routes';
import { HTTP_CODES } from './Constants/HttpStatusCodes';
import * as responseGenerator from './Helper/ResponseGenerator';

// Server Configuration here
const app: Application = express();

// Adding body-parser config
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Adding the Routes
app.use(router);

// Adding CORS
app.use((req: Request, res: Response, next: NextFunction) => {
  res.header('Access-Control-Allow-Origin', SERVER.CORS_LOCATION);
  res.header('Access-Control-Allow-Headers', SERVER.CORS_HEADERS);
  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'GET POST PUT DELETE');
    return res.status(HTTP_CODES.OK).json({});
  }
  next();
});

// To Test if eveything is working
router.get('/', (req: Request, res: Response) => {
  res
    .status(HTTP_CODES.OK)
    .json(
      responseGenerator.SuccessResponseCreator(
        HTTP_CODES.OK,
        `Please use <address>${SERVER.BASE_URL}`
      )
    );
});

// Starting the Server here
app.listen(SERVER.PORT, () => {
  createServerStartedLog();
});

// Creates Server Started Dialogue when starting server
const createServerStartedLog = () => {
  const divider = chalk.green(
    chalk.bold('-----------------------------------')
  );
  // This is added to distinguish easily if using log file
  logger.info(divider);
  logger.info(chalk.green(chalk.bold('Server Started !!')));
  logger.info('Access URLs : ');
  logger.info(
    chalk.underline(
      chalk.bold(`Localhost -> ${SERVER.HOSTNAME}:${SERVER.PORT}`)
    )
  );
  logger.info(
    chalk.underline(
      chalk.bold(`On Network -> ${ip.address() + ':' + SERVER.PORT}`)
    )
  );
  logger.info(divider);
};

export { app };
