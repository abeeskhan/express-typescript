import logger from '../Logger';

export const SuccessResponseCreator = (statusCode: number, message: string) => {
  try {
    return {
      code: statusCode,
      message: message,
    };
  } catch (error) {
    logger.error(`Error When Creating SuccessResponse -> ${error}`);
  }
};

export const ErrorResponseCreator = (statusCode: number, error: string) => {
  try {
    return {
      code: statusCode,
      message: 'Error Occured',
      error: error,
    };
  } catch (error) {
    logger.error(`Error When Creating ErrorResponse -> ${error}`);
  }
};
