import winston, { Logger } from 'winston';
import { LOGGING } from '../Constants/Config';
import chalk from 'chalk';

const logger: Logger = winston.createLogger({
  format: winston.format.json(),
  transports: [
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.timestamp({
          // Date format for the logger here.
          format: 'DD/MM/YYYY hh:mm:ss A',
        }),

        // Format logger Style that needs to be shown in the Console here
        winston.format.printf(
          (info) =>
            `${
              info.level === 'error'
                ? chalk.bold(chalk.red(info.level.toUpperCase()))
                : chalk.bold(chalk.magenta(info.level.toUpperCase()))
            }: ${chalk.underline(chalk.blue(info.timestamp))} -> ${
              info.message
            }`
        )
      ),
    }),
  ],
});

if (LOGGING.ENABLED) {
  // Info Logger file that logs eveything
  logger.add(
    new winston.transports.File({
      level: 'info',
      filename: `${LOGGING.FOLDER}/${LOGGING.INFO_LOG_FILE}`,
      format: winston.format.combine(
        winston.format.timestamp({
          format: 'DD-MM-YYYY hh:mm:ss A',
        }),
        winston.format.printf(
          (info) =>
            `${info.level.toUpperCase()}: ${info.timestamp} -> ${info.message}`
        )
      ),
    })
  );

  // Error Logger file that logs only Errors
  logger.add(
    new winston.transports.File({
      filename: `${LOGGING.FOLDER}/${LOGGING.ERROR_LOG_FILE}`,
      level: 'error',
      format: winston.format.combine(
        winston.format.timestamp({
          format: 'DD-MM-YYYY hh:mm:ss A',
        }),
        winston.format.printf(
          (info) =>
            `${info.level.toUpperCase()}: ${info.timestamp} -> ${info.message}`
        )
      ),
    })
  );
}

export default logger;
