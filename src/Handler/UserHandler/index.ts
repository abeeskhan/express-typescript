import { Request, Response } from 'express';
import { userService } from '../../Service';
import logger from '../../Logger';
import {
  SuccessResponseCreator as successResponse,
  ErrorResponseCreator as errorResponse,
} from '../../Helper/ResponseGenerator';
import { HTTP_CODES as codes } from '../../Constants/HttpStatusCodes';

export const getAllUsers = async (req: Request, res: Response) => {
  logger.info(
    `Hit /api${req.url}, Body -> \n ${JSON.stringify(req.body, null, 1)}`
  );
  try {
    res
      .status(codes.OK)
      .json(successResponse(codes.OK, await userService.getAllUsers()));
  } catch (error) {
    logger.error(`Error Occured in getAllUsers (/api${req.url}) -> ${error}`);
    res.status(codes.BAD_REQUEST).json(errorResponse(codes.BAD_REQUEST, error));
  }
};
