# Express Typescript Server Boilerplate

** Modules Used**
** 1. Nodemon - To Run the Development Server. **
** 2. ts-node - To Run Typescript files in Nodejs. **
** 3. express - To Create the Express Server. **
** 4. winston - Logging Framework. **
** 5. body-parser - Parsing JSON. **

\*\* To Configure Server, Please go to Config.ts
